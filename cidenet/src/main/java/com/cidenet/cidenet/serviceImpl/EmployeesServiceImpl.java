package com.cidenet.cidenet.serviceImpl;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.cidenet.cidenet.DTO.EmployeesDTO;
import com.cidenet.cidenet.entities.Employees;
import com.cidenet.cidenet.repository.EmployeesRepository;
import com.cidenet.cidenet.service.EmployeesService;

@Service
public class EmployeesServiceImpl implements EmployeesService{
	@Autowired
	EmployeesRepository employeeRepo;
	
	/*
	 * Agrega un nuevo empleado
	 * ingresa estructura tipo empleado (lease la entidad employees) 
	 * retorna true si se agrega exitosamente
	 * retorna false si no se agrega
	 */
	@Override
	public boolean addEmployee(Employees employee) {
		int lastid = employeeRepo.getlastid();
		lastid += 1;
		String firstname = employee.getFirst_name().toLowerCase();
		String surname = employee.getSurname().toLowerCase();
		String id = String.valueOf(lastid);
		String dominio;
		

		if(employee.getCountry_employment().toLowerCase().equals("colombia")) {

			dominio = "cidenet.com.co";
		}
		else {
			dominio = "cidenet.com.us";
		}
		employee.setEmail(firstname + "." + surname + "." + id + "@" + dominio);
		
		employeeRepo.save(employee);
		
		return false;
	}

	
	/*
	 *Busca empleado por string
	 *ingresa: empleado con estructura json, pagina para el paginado, selección 1 o 2
	 *retorna: coincidencias en la db 
	 */

	@Override
	public Iterable<EmployeesDTO> sortByName(Employees employee, int page, int select) {
		
		
		
		switch(select) {
			case 1:
				String filter = employee.getFilter();
				Iterable<Employees> employees = employeeRepo.findByFilterString(filter, PageRequest.of(page, 10));
	
				Collection<EmployeesDTO> emplDTO = new HashSet<EmployeesDTO>();
				employees.forEach((c)-> {
	
					emplDTO.add(new EmployeesDTO(c));
				});
				return emplDTO;
			case 2:
				int filter2 = employee.getFilter2();
				Iterable<Employees> employees2 = employeeRepo.findByFilterInt(filter2, PageRequest.of(page, 10));
	
				Collection<EmployeesDTO> emplDTO2 = new HashSet<EmployeesDTO>();
				employees2.forEach((c)-> {
	
					emplDTO2.add(new EmployeesDTO(c));
				});
				return emplDTO2;
		}
		return null;
		
	}

	/*
	 * actualizar empleado
	 * ingresa: datos nuevos de empleado por json, id de empleado
	 * retorna: true si se hizo la actualización o false si no se hizo.
	 */

	@Override
	public boolean updateById(Employees employee, int id) {
		
		Employees empl = employeeRepo.findByIdUniq(id);

		int flag = 0;
		if (employee.getFirst_name() != null) {
			empl.setFirst_name(employee.getFirst_name());
			flag = 1;
		}
		if (employee.getSurname() != null) {

			empl.setSurname(employee.getSurname());
			flag = 1;
		}
		if (employee.getLast_surname() != null) {
			empl.setLast_surname(employee.getLast_surname());
			flag = 1;
		}
		if (employee.getOther_name() != null) {
			empl.setOther_name(employee.getOther_name());
			flag = 1;
		}
		if (employee.getCountry_employment() != null) {
			empl.setCountry_employment(employee.getCountry_employment());
			flag = 1;
		}
		if (employee.getDocument_number() != null) {
			empl.setDocument_number(employee.getDocument_number());
			flag = 1;
		}
		if (employee.getDocument_type_id() != 0) {
			empl.setDocument_type_id(employee.getDocument_type_id());
			flag = 1;
		}
		if (employee.getDate_of_admission() != null) {
			empl.setDate_of_admission(employee.getDate_of_admission());
			flag = 1;
		}
		if (employee.getWork_area() != null) {
			empl.setWork_area(employee.getWork_area());
			flag = 1;
		}
		employeeRepo.save(empl);
		
		Employees empl2 = employeeRepo.findByIdUniq(id);

		String firstname = empl2.getFirst_name().toLowerCase();
		String surname = empl2.getSurname().toLowerCase();
		int idn = id;
		String dominio;
		

		if(empl2.getCountry_employment().toLowerCase().equals("colombia")) {
			dominio = "cidenet.com.co";
		}
		else {
			dominio = "cidenet.com.us";
		}
		empl2.setEmail(firstname + "." + surname + "." + idn + "@" + dominio);
		
		employeeRepo.save(empl2);
		
		if (flag == 0) {
			return false;
		}
		return true;
	}
	
	/*
	 * borra empleado por id
	 * ingresa: id de empleado
	 * retorna: true si se borró
	 */

	@Override
	public boolean deleteById(int id) {
		
		employeeRepo.deleteById(id);
		Employees empl = employeeRepo.findByIdUniq(id);
		if (empl == null) {
			return true;
		}
		
		return false;
		
	}

		

}
