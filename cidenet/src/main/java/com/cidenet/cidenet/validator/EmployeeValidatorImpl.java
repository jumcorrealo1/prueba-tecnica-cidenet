package com.cidenet.cidenet.validator;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Component;

import com.cidenet.cidenet.entities.Employees;
import com.cidenet.cidenet.utils.ApiUnprocessableEntity;

@Component
public class EmployeeValidatorImpl implements EmployeeValidator{

	@Override
	public Boolean validator(Employees employee) throws ApiUnprocessableEntity {
		
		
		/*
		 * Funciones para comprobar si se ingresa un nuevo empleado correctamente
		 * retorna: string con el dato errado y un false;
		 * las comprobaciones se hacen con expresiones regulares
		 */
		
		
		if(employee.getFirst_name() == null || employee.getFirst_name().isEmpty()) {
			System.out.print("Primer nombre obligatorio");
			return false;
		} else {
			String regexfirstname = "(?i)(^[A-Z])((?![ .,'-]$)[A-Z .,'-]){0,20}$";
			Pattern patternname = Pattern.compile(regexfirstname);
			Matcher matcher = patternname.matcher(employee.getFirst_name());
			if(!matcher.matches()) {
				System.out.println("nombre muy largo o formato invalido");
				return false;
			}
		}
		if(employee.getOther_name() != null) {
			String regexothername = "(?i)(^[A-Z])((?![ .,'-]$)[A-Z .,'-]){0,50}$";
			Pattern patternname = Pattern.compile(regexothername);
			Matcher matcher = patternname.matcher(employee.getFirst_name());
			if(!matcher.matches()) {
				System.out.println("segundo nombre muy largo o formato invalido");
				return false;
			}
		}
		
		if(employee.getSurname() == null || employee.getSurname().isEmpty()) {
			System.out.print("Primer apellido obligatorio");
			return false;
		} else {
			String regexsurname = "(?i)(^[A-Z])((?![ .,'-]$)[A-Z .,'-]){0,20}$";
			Pattern patternname = Pattern.compile(regexsurname);
			Matcher matcher = patternname.matcher(employee.getSurname());
			if(!matcher.matches()) {
				System.out.println("primer apellido muy largo o formato invalido");
				return false;
			}
		}
		if(employee.getLast_surname() != null) {
			
			String regexfirstname = "(?i)(^[A-Z])((?![ .,'-]$)[A-Z .,'-]){0,20}$";
			Pattern patternname = Pattern.compile(regexfirstname);
			Matcher matcher = patternname.matcher(employee.getFirst_name());
			if(!matcher.matches()) {
				System.out.println("segundo apellido muy largo o formato invalido");
				return false;
			}
		}
		if(employee.getDate_of_admission() == null) {
			System.out.println("fecha de admisión obligatoria");
			return false;
		} else {
			LocalDateTime date = employee.getDate_of_admission().toLocalDateTime();
			LocalDateTime date2 = LocalDateTime.now();
			LocalDateTime date3= date2.minusMonths(1);
			boolean datebool = date.isBefore(date2) && date.isAfter(date3);
			if (date.isAfter(date2)) {
				System.out.println("la fecha debe ser antes de la actual");
				return false;
			}else if(!datebool) {
				System.out.println("la fecha debe estar dentro del rango requerido");
				return false;
				
			}
		
			
		}
		
		if(employee.getDocument_number() == null || employee.getDocument_number().isEmpty()) {
			System.out.print("número de identificación obligatorio");
			return false;
		} else {
			String regexsurname = "(?i)(^[A-Za-z0-9]*)((?![-]$)[a-z .,'-]){0,20}$";
			Pattern patternname = Pattern.compile(regexsurname);
			Matcher matcher = patternname.matcher(employee.getSurname());
			if(!matcher.matches()) {
				System.out.println("numero de identificación muy largo o formato invalido");
				return false;
			}
			return true;
		}
		
		
			
		

	}

}
