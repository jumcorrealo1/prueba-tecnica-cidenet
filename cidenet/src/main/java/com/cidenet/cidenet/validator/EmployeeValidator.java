package com.cidenet.cidenet.validator;

import org.springframework.stereotype.Service;

import com.cidenet.cidenet.entities.Employees;
import com.cidenet.cidenet.utils.ApiUnprocessableEntity;


@Service
public interface EmployeeValidator {
	
	public Boolean validator(Employees employee) throws ApiUnprocessableEntity;
	
}
