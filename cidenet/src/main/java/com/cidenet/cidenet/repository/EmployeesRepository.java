package com.cidenet.cidenet.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.cidenet.cidenet.entities.Employees;

/**
 * encargada de ingresar a la db y hacer cambios gracias a Crud
 * @author Juan Correa
 *
 */

public interface EmployeesRepository extends CrudRepository<Employees, Integer>{

	
	/*
	 * Trae el ultimo id registrado en la db
	 */
	@Query(value = "select MAX(employee_id) from employees ", nativeQuery = true)
	public int getlastid();
	
	
	
	/*
	 * Filtra la busqueda de empleado por string
	 */
	@Query(value = "select * from employees "
			+ "where first_name LIKE %:filter% "
			+ "or surname LIKE %:filter% "
			+ "or last_surname LIKE %:filter% "
			+ "or other_name LIKE %:filter% "
			+ "or document_number LIKE %:filter%", nativeQuery = true)
	public Page<Employees> findByFilterString(@Param("filter") String filter, Pageable page);


	/*
	 * Fitra la busqueda de empleado por el tipo de documento
	 */
	@Query(value = "select * from employees "
			+ "where document_type_id = :filter", nativeQuery = true)
	public Page<Employees> findByFilterInt(@Param("filter") int filter, Pageable of);

	/*
	 * busca empleado por su id
	 */
	@Query(value = "select * from employees "
			+ "where employee_id = :id", nativeQuery = true)
	public Employees findByIdUniq(@Param("id") int id);
	

}
