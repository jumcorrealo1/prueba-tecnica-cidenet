package com.cidenet.cidenet.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/*
 * Persostencia para la tabla "document_types"
 * 
 */
@Entity
@Table(name = "document_types")
public class Document_types {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "document_type_id")
	private int document_type_id;
	
	@Column(name = "type")
	private String type;
	


	public int getDocument_type_id() {
		return document_type_id;
	}

	public void setDocument_type_id(int document_type_id) {
		this.document_type_id = document_type_id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	

}
