package com.cidenet.cidenet.entities;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * Persistencia para la tabla employees
 * @author Juan Correa
 *
 */
@Entity
@Table(name = "employees")
public class Employees {

	
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = "employee_id")
		private int employee_id;
		
		@Column(name = "document_type_id")
		private int document_type_id;
		
		@Column(name = "surname")
		private String surname;
		
		@Column(name = "last_surname")
		private String last_surname;
		
		@Column(name = "first_name")
		private String first_name;
		
		@Column(name = "other_name")
		private String other_name;
		
		@Column(name = "country_employment")
		private String country_employment;
		
		@Column(name = "document_number")
		private String document_number;
		
		@Column(name = "email")
		private String email;
		
		@Column(name = "date_of_admission")
		private Timestamp date_of_admission;
		
		@Column(name = "work_area")
		private String work_area;
		
		@Column(name = "created_at")
		private Timestamp created_at;
		
		@Transient
		private String filter;
		
		@Transient
		private int filter2;
		
		
		
		public String getFilter() {
			return filter;
		}
		public void setFilter(String filter) {
			this.filter = filter;
		}
		public int getFilter2() {
			return filter2;
		}
		public void setFilter2(int filter2) {
			this.filter2 = filter2;
		}
		public Timestamp getDate_of_admission() {
			return date_of_admission;
		}
		public String getDocument_number() {
			return document_number;
		}
		public void setDocument_number(String document_number) {
			this.document_number = document_number;
		}
		public String getFirst_name() {
			return first_name;
		}
		public void setFirst_name(String first_name) {
			this.first_name = first_name;
		}
		public void setDate_of_admission(Timestamp date_of_admission) {
			this.date_of_admission = date_of_admission;
		}
		public String getWork_area() {
			return work_area;
		}
		public void setWork_area(String work_area) {
			this.work_area = work_area;
		}
		public Timestamp getCreated_at() {
			return created_at;
		}
		public void setCreated_at(Timestamp created_at) {
			this.created_at = created_at;
		}
		public Timestamp getModified_at() {
			return modified_at;
		}
		public void setModified_at(Timestamp modified_at) {
			this.modified_at = modified_at;
		}

		@Column(name = "modified_at")
		private Timestamp modified_at;
		
		@PrePersist
	    public void prePersist() {
	        Date date = new Date();
	        long time = date.getTime();
	        this.created_at = new Timestamp(time);
	        this.modified_at = new Timestamp(time);
	    }
		@PreUpdate
	    public void preUpdate() {
	    	 Date date = new Date();
	         long time = date.getTime();
	         this.modified_at = new Timestamp(time);
	    }
		

		public int getEmployee_id() {
			return employee_id;
		}

		public void setEmployee_id(int employee_id) {
			this.employee_id = employee_id;
		}

		public int getDocument_type_id() {
			return document_type_id;
		}

		public void setDocument_type_id(int document_type_id) {
			this.document_type_id = document_type_id;
		}

		public String getSurname() {
			return surname;
		}

		public void setSurname(String surname) {
			this.surname = surname;
		}

		public String getLast_surname() {
			return last_surname;
		}

		public void setLast_surname(String last_surname) {
			this.last_surname = last_surname;
		}

		public String getOther_name() {
			return other_name;
		}

		public void setOther_name(String other_name) {
			this.other_name = other_name;
		}

		public String getCountry_employment() {
			return country_employment;
		}

		public void setCountry_employment(String country_employment) {
			this.country_employment = country_employment;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}
		
		
		
		
}
