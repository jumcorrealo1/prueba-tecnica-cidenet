package com.cidenet.cidenet.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;

import com.cidenet.cidenet.DTO.EmployeesDTO;
import com.cidenet.cidenet.entities.Employees;
import com.cidenet.cidenet.utils.ApiUnprocessableEntity;

@CrossOrigin(origins = "*", maxAge = 3600)
public interface EmployeesController {

	public Boolean addEmployee(Employees employee) throws ApiUnprocessableEntity;

	public Iterable<EmployeesDTO> consult(Employees employee, int page, int select);

	public boolean updateEmployee(Employees employee, int id);

	public boolean deleteEmployee(int id);
	
	

}
