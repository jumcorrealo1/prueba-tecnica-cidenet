package com.cidenet.cidenet.controllersImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cidenet.cidenet.DTO.EmployeesDTO;
import com.cidenet.cidenet.controllers.EmployeesController;
import com.cidenet.cidenet.entities.Employees;
import com.cidenet.cidenet.service.EmployeesService;
import com.cidenet.cidenet.utils.ApiUnprocessableEntity;
import com.cidenet.cidenet.validator.EmployeeValidatorImpl;

@RestController
public class EmployeesControllerImpl implements EmployeesController{
	
	@Autowired
	EmployeesService employeesServ;
	@Autowired
	EmployeeValidatorImpl employeeValidator;
	
	
	/*
	 * Agregare un nuevo usuario
	 * Recibe un json con el nuevo empleado
	 */
	@RequestMapping(value = "/employee/create", method = RequestMethod.POST, produces = "application/json")
	@Override
	public  Boolean addEmployee(@RequestBody Employees employee) throws ApiUnprocessableEntity{
		if(this.employeeValidator.validator(employee)) {
		employeesServ.addEmployee(employee);
		return true;
			
		}else {
			return false;
		}
		
	}
	/*
	 * Buscar usuario
	 * Ingresa en select 1 si quiere buscar por string (incluye numero de documento)
	 * Ingresa en select 2 si quiere buscar por tipo de documento 
	 */
	@RequestMapping(value = "/employee/consult/{select}/{page}", method = RequestMethod.GET, produces = "application/json")
	@Override
	public Iterable<EmployeesDTO> consult(@RequestBody Employees employee,@PathVariable("page") int page,@PathVariable("select") int select) {
		

		return employeesServ.sortByName(employee, page, select);

	}
	
	/*
	 * Actualizar usuario
	 * ingresar el id del usuario a actualizar
	 * ingresar por el body un json con los datos a actualizar
	 */

	@RequestMapping(value = "/employee/update/{id}", method = RequestMethod.POST, produces = "application/json")
	@Override
	public boolean updateEmployee(@RequestBody Employees employee,@PathVariable("id") int id) {
		
		return employeesServ.updateById(employee, id);

	}
	
	/*
	 * Borrar usuario
	 * Ingresar el id del usuario que se quiere borrar
	 */
	@RequestMapping(value = "/employee/delete/{id}", method = RequestMethod.POST, produces = "application/json")
	@Override
	public boolean deleteEmployee(@PathVariable("id") int id) {
		
		return employeesServ.deleteById(id);

	}


}
