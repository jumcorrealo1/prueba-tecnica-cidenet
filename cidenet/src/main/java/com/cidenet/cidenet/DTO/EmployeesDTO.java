package com.cidenet.cidenet.DTO;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Transient;

import com.cidenet.cidenet.entities.Employees;

public class EmployeesDTO implements Serializable {
	
	
	private int employee_id;
	private int document_type_id;
	private String surname;
	private String last_surname;
	private String first_name;
	private String other_name;
	private String country_employment;
	private String email;
	private Timestamp date_of_admission;
	private String work_area;
	private Timestamp created_at;
	
	
	/*
	 * Estructura para crear el DTO
	 * 
	 */
	
	public EmployeesDTO(Employees empl) {
		super();
		this.employee_id = empl.getEmployee_id();
		this.document_type_id = empl.getDocument_type_id();
		this.surname = empl.getSurname();
		this.last_surname = empl.getLast_surname();
		this.first_name = empl.getFirst_name();
		this.other_name = empl.getOther_name();
		this.country_employment = empl.getCountry_employment();
		this.email = empl.getEmail();
		this.date_of_admission = empl.getDate_of_admission();
		this.work_area = empl.getWork_area();
		this.created_at = empl.getCreated_at();
	}
	
	public int getEmployee_id() {
		return employee_id;
	}
	public void setEmployee_id(int employee_id) {
		this.employee_id = employee_id;
	}
	public int getDocument_type_id() {
		return document_type_id;
	}
	public void setDocument_type_id(int document_type_id) {
		this.document_type_id = document_type_id;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getLast_surname() {
		return last_surname;
	}
	public void setLast_surname(String last_surname) {
		this.last_surname = last_surname;
	}
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getOther_name() {
		return other_name;
	}
	public void setOther_name(String other_name) {
		this.other_name = other_name;
	}
	public String getCountry_employment() {
		return country_employment;
	}
	public void setCountry_employment(String country_employment) {
		this.country_employment = country_employment;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Timestamp getDate_of_admission() {
		return date_of_admission;
	}
	public void setDate_of_admission(Timestamp date_of_admission) {
		this.date_of_admission = date_of_admission;
	}
	public String getWork_area() {
		return work_area;
	}
	public void setWork_area(String work_area) {
		this.work_area = work_area;
	}
	public Timestamp getCreated_at() {
		return created_at;
	}
	public void setCreated_at(Timestamp created_at) {
		this.created_at = created_at;
	}

	
}
