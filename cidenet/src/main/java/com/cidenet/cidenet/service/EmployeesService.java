package com.cidenet.cidenet.service;

import com.cidenet.cidenet.DTO.EmployeesDTO;
import com.cidenet.cidenet.entities.Employees;


public interface EmployeesService {

	public boolean addEmployee(Employees employee);

	public Iterable<EmployeesDTO> sortByName(Employees employee, int page, int select);

	public boolean updateById(Employees employee, int id);

	public boolean deleteById(int id);

}
