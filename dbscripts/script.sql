-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema cidnet
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema cidnet
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `cidnet` DEFAULT CHARACTER SET latin1 ;
USE `cidnet` ;

-- -----------------------------------------------------
-- Table `cidnet`.`document_types`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cidnet`.`document_types` (
  `document_type_id` INT(11) NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`document_type_id`))
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `cidnet`.`employees`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cidnet`.`employees` (
  `employee_id` INT(11) NOT NULL AUTO_INCREMENT,
  `surname` VARCHAR(50) NOT NULL,
  `last_surname` VARCHAR(50) NULL DEFAULT NULL,
  `first_name` VARCHAR(50) NOT NULL,
  `other_name` VARCHAR(50) NULL DEFAULT NULL,
  `country_employment` VARCHAR(45) NOT NULL,
  `document_type_id` INT(11) NULL DEFAULT NULL,
  `email` VARCHAR(300) NOT NULL,
  `date_of_admission` DATETIME NOT NULL,
  `work_area` VARCHAR(100) NOT NULL,
  `state` TINYINT(4) NOT NULL DEFAULT '1',
  `created_at` DATETIME NOT NULL,
  `modified_at` DATETIME NOT NULL,
  `document_number` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`employee_id`))
ENGINE = InnoDB
AUTO_INCREMENT = 24
DEFAULT CHARACTER SET = latin1;

CREATE UNIQUE INDEX `email_UNIQUE` ON `cidnet`.`employees` (`email` ASC) VISIBLE;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
